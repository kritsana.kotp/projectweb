<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Schedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule',function (Blueprint $table){
            $table->increments('sid');
           $table->integer('uid')->unsigned();
           $table->foreign('uid')->references('id')->on('user')->onDelete('cascade');
           $table->integer('did')->unsigned();
           $table->foreign('did')->references('did')->on('doctors')->onDelete('cascade');
           $table->integer('confirm');
           $table->date('date');
           $table->time('time');
           $table->integer('round');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}
