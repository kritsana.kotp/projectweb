<?php

namespace App\Http\Controllers;

use App\doctorsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Return_;

class DoctorsController extends Controller
{
    public function deleteDoctor(Request $request){
        $data = $request->all();
        $did = $data['did'];
        DB::table('doctors')->where('did',$did)->delete();
        return view('admin');
    }
    public function fixDoctor(Request $request){
        $data = $request->all();
        $id = $data['doctor'];
        $result = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'expertise' => $data['expertise'],
            'phone' => $data['phone']
        );
        DB::table('doctors')->where('did',$id)->update($result);
        return view('admin');
    }
    public function doctorPickTime(Request $request){
        $data = $request->all();
        $uid = $data['user'];
        $did = $data['did'];
        $date = $data['date'];
        $time = $data['time'];
        $array = array(
            'uid' => $uid,
            'did' => $did
        );
        $before = DB::table('schedule')->where($array)->get();
        $count = count($before);
        if($count>0){
            $round = DB::table('schedule')->select(max(['round']))->where($array)->orderBy('round','desc')->get();
            $round = $round[0]->round + 1;
            $array = array(
                'uid' => $uid,
                'did' => $did,
                'date' => $date,
                'time' => $time,
                'round' => $round,
                'confirm' => 1
            );
            DB::table('schedule')->insert($array);
        }else{
            $array = array(
                'uid' => $uid,
                'did' => $did,
                'date' => $date,
                'time' => $time,
                'round' => 1,
                'confirm' => 1
            );
            DB::table('schedule')->insert($array);
        }
        $doc = DB::table('doctors')->where('did',$did)->get();
        $doc = $doc[0];
        return view('doctor',['doc'=>$doc]);
    }
    public function confirmReq(Request $request){
        $data =  $request->all();
        $sid = $data['sid'];
        $did = $data['did'];
        $array = array(
            'confirm' => 1
        );
        DB::table('schedule')->where('sid',$sid)->update($array);
        $doc = DB::table('doctors')->where('did',$did)->get();
        $doc = $doc[0];
        return view('doctor',['doc'=>$doc]);
    }
    public function updateDoctor(Request $request){
        $data = $request->all();
        $id = $data['id'];
        $result = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'expertise' => $data['expertise'],
            'phone' => $data['phone']
        );
        DB::table('doctors')->where('did',$id)->update($result);
        $doc = DB::table('doctors')->where('did',$id)->get();
        $doc = $doc[0];
        return view('doctor',['doc'=>$doc]);
    }
    public function pickTime(Request $request){
        $data = $request->all();
        $uid = $data['uid'];
        $did = $data['doctor'];
        $date = $data['date'];
        $time = $data['time'];
        $array = array(
            'uid' => $uid,
            'did' => $did
        );
        $before = DB::table('schedule')->where($array)->get();
        $count = count($before);
        if($count>0){
            $round = DB::table('schedule')->select(max(['round']))->where($array)->orderBy('round','desc')->get();
            $round = $round[0]->round + 1;
            $array = array(
                'uid' => $uid,
                'did' => $did,
                'date' => $date,
                'time' => $time,
                'round' => $round,
                'confirm' => 0
            );
            DB::table('schedule')->insert($array);
        }else{
            $array = array(
                'uid' => $uid,
                'did' => $did,
                'date' => $date,
                'time' => $time,
                'round' => 1,
                'confirm' => 0
            );
            DB::table('schedule')->insert($array);
        }
        $user = DB::table('user')->where('id',$uid)->get();
        $user = $user[0];
        $doctors = DB::table('doctors')->get();
        return view('users',['user'=>$user,'doctors'=>$doctors,'schedule'=>null]);


    }
    public function selectDoctor(Request $request){
        //return "SUCCESS";
        $data = $request->all();
        $name = $data['doctor'];
        $doctor = DB::table('doctors')->where('name',$name)->get();
        $id = $data['id'];
        $user = DB::table('user')->where('id',$id)->get();
        $user = $user[0];
        $doctors = DB::table('doctors')->get();
        $a = array(
            'did' => $id,
            'uid' => $doctor[0]->did,
            'confirm' => 1,
        );
        $schedule = DB::table('schedule')->where($a)->get();
        return view('users',['schedule'=>$schedule,'user'=>$user,'doctors'=>$doctors]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\doctorsModel  $doctorsModel
     * @return \Illuminate\Http\Response
     */
    public function show(doctorsModel $doctorsModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\doctorsModel  $doctorsModel
     * @return \Illuminate\Http\Response
     */
    public function edit(doctorsModel $doctorsModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\doctorsModel  $doctorsModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, doctorsModel $doctorsModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\doctorsModel  $doctorsModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(doctorsModel $doctorsModel)
    {
        //
    }
}
