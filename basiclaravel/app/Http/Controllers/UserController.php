<?php

namespace App\Http\Controllers;

use App\User;
use App\userModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function deleteUser(Request $request){
        $data = $request->all();
        $uid = $data['uid'];
        DB::table('user')->where('id',$uid)->delete();
        return view('admin');
    }
    public function fixUser(Request $request){
        $data = $request->all();
        $uid = $data['user'];
        $result = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'address' => $data['address'],
            'phone' => $data['phone']
        );
        DB::table('user')->where('id',$uid)->update($result);
        return view('admin');
    }
    public function updateUser(Request $request){
        $data = $request->all();
        $id = $data['id'];
        $result = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'address' => $data['address'],
            'phone' => $data['phone']
        );
        DB::table('user')->where('id',$id)->update($result);
        $user = DB::table('user')->where('id',$id)->get();
        $user = $user[0];
        $doctors = DB::table('doctors')->get();
        return view('users',['user'=>$user,'doctors'=>$doctors,'schedule'=>null]);
    }
    public function checkLogin(Request $request){
        $data = $request->all();
        $email = $data['email'];
        $pass = $data['password'];
        $array = array(
            'email' => $email,
            'password' => $pass
        );
        $ans = DB::table('user')->where($array)->get();
        $row_count = count($ans);
        $doctors = DB::table('doctors')->get();
        if($email == 'ad@min' && $pass == '@min'){
            return view('admin');
        }
        else if($row_count == 1){
            $message = "ok user";
            $user = $ans[0];
            return view('users',['user'=>$user,'doctors'=>$doctors,'schedule'=>null]);
        }else{
            $ans = DB::table('doctors')->where('email',$email)->get();
            $row_count = count($ans);
            if($row_count== 1 && $pass == 'doctor'){
                $doc = $ans[0];
                return view('doctor',['doc'=>$doc]);
            }else{
                return view('index');
            }
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new userModel();
        $user->setAttribute('name',$request->get('name'));
        $user->setAttribute('email',$request->get('email'));
        $user->setAttribute('password',$request->get('password'));
        $user->setAttribute('address',$request->get('address'));
        $user->setAttribute('phone',$request->get('phone'));

        if($user->save()){
            return view('users');
        }

       // return redirect()->route('user.index')->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = new userModel();
        if(!$user->getAttributeValue($id)){
            return redirect()->back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function edit(userModel $userModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, userModel $userModel)
    {
        return "Yes";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\userModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(userModel $userModel)
    {

    }
}
