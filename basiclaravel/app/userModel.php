<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userModel extends Model
{
    protected $table = "user";
    protected  $fillable = ['name','email','password','address','phone'];
}
