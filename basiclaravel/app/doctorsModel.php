<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctorsModel extends Model
{
    protected $table = 'doctors';
    protected  $fillable = ['uid','did','count','date','time'];
}
