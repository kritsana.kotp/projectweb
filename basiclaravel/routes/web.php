<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/index', function () {
    return view('index');
});
Route::get('/users','DoctorsController@dropList');
Route::get('/doctor', function () {
    return view('doctor');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('doctors','DoctorsController');
Route::resource('user','UserController');
Route::post('/checkLogin','UserController@checkLogin');
Route::post('/updateUser','UserController@updateUser');
Route::post('/selectDoctor','DoctorsController@selectDoctor');
Route::post('/pickTime','DoctorsController@pickTime');
Route::post('/updateDoctor','DoctorsController@updateDoctor');
Route::post('/confirmReq','DoctorsController@confirmReq');
Route::post('/doctorPickTime','DoctorsController@doctorPickTime');
Route::post('/fixUser','UserController@fixUser');
Route::post('/fixDoctor','DoctorsController@fixDoctor');
Route::post('/deleteDoctor','DoctorsController@deleteDoctor');
Route::post('/deleteUser','UserController@deleteUser');
