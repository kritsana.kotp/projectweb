<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>DOCTOR - Responsive HTML &amp; Bootstrap Template</title>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,800,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=BenchNine:300,400,700' rel='stylesheet' type='text/css'>
    <script src="js/modernizr.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->    <style>

        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>
</head>
<body>
<!-- ====================================================
    header section -->
<header class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-5 header-logo">
                <br>
                <a href="index"><img src="img/logo.png" alt="" class="img-responsive logo"></a>
            </div>
            <div class="col-md-7">
                <nav class="navbar navbar-default">
                    <div class="container-fluid nav-bar">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                            <ul class="nav navbar-nav navbar-right">
                                <li><a class="menu" onclick="openCity(event,'London')">ตารางนัด </a></li>
                                <li><a class="menu" onclick="openCity(event,'Paris')">คำขอนัด</a></li>
                                <li><a class="menu" onclick="openCity(event,'Request')">นัดผู้ป่วย</a></li>
                                <li><a class="menu" onclick="openCity(event,'Doctordetail')">แก้ไขข้อมูลส่วนตัว</a></li>
                                <li><a class="menu" href="index" >LOGOUT</a></li>
                            </ul>
                        </div><!-- /navbar-collapse -->
                    </div><!-- / .container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</header> <!-- end of header area -->

<br><br><br><br><br><br><br>
<div class="container">
    <?php
    echo "<div class=\"card-header\"><h2>Hi!  Doctor ".$doc->name."</h2></div>";
    ?>
</div>

<div id="London" class="tabcontent">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h2>{{ __('ตารางนัดผู้ป่วย') }}</h2></div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ชื่อผู้ป่วย</th>
                    <th>วัน</th>
                    <th>เวลา</th>
                    <th>ครั้งที่</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $array = array(
                    'did' => $doc->did,
                    'confirm' => 1
                );
                $schedule = \Illuminate\Support\Facades\DB::table('schedule')->where($array)->get();
                if($schedule != null){
                    foreach($schedule as $schedule){
                        $username = \Illuminate\Support\Facades\DB::table('user')->where('id',$schedule->uid)->get();
                        echo "<tr><td>".$username[0]->name."</td><td>".$schedule->date."</td><td>".$schedule->time."</td><td>".$schedule->round."</td></tr>";
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="Paris" class="tabcontent">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h2>{{ __('คำขอ') }}</h2></div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ชื่อผู้ป่วย</th>
                    <th>วัน</th>
                    <th>เวลา</th>
                    <th>ครั้งที่</th>
                    <th>ยืนยัน</th>
                </tr>
                </thead>
                <tbody>
                <?php
                        $array = array(
                            'did' => $doc->did,
                            'confirm' => 0
                        );
                $schedule = \Illuminate\Support\Facades\DB::table('schedule')->where($array)->get(); ?>
                @if($schedule != null)
                    <form method="POST" action="/confirmReq">
                        @csrf
                        <input type="hidden" name="did" value="<?=$doc->did?>">
                    @foreach($schedule as $schedule)
                            <input type="hidden" name="sid" value="<?=$schedule->sid?>">
                        <?= $username = \Illuminate\Support\Facades\DB::table('user')->where('id',$schedule->uid)->get(); ?>
                        <tr><td><?= $username[0]->name ?></td><td><?=$schedule->date?></td><td><?=$schedule->time?></td><td><?=$schedule->round?></td>
                            <td>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Yes') }}
                                </button>
                            </td>
                        </tr>";
                    @endforeach
                    </form>
                @endif

                </tbody>
            </table>

        </div>
    </div>
</div>
<div id="Request" class="tabcontent">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h2>{{ __('นัดผู้ป่วย') }}</h2></div>

                    <div class="card-body">
                        <form method="POST" action="/doctorPickTime">

                            @csrf
                            <input type="hidden" name="did" value="<?=$doc->did?>">
                            <div class="form-group row">
                                <div class="col-12 col-md-8">
                                    <select name="user" id="user" class="form-control input-lg dynamic" >
                                        <option value="">เลือกผู้ป่วย</option>
                                        <?php $users = \Illuminate\Support\Facades\DB::table('user')->get(); ?>
                                        @foreach($users as $users)
                                            <?php echo "<option value='".$users->id."'>".$users->name."</option>"; ?>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                                <div class="col-md-6">
                                    <input id="date" type="text" placeholder="YYYY-MM-DD" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" required autofocus>

                                    @if ($errors->has('date'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>

                                <div class="col-md-6">
                                    <input id="time" type="text" name="time" placeholder="HH:MM" class="form-control{{ $errors->has('time') ? ' is-invalid' : '' }}"  required>

                                    @if ($errors->has('time'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Sent') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="Doctordetail" class="tabcontent">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h2>{{ __('ข้อมูส่วนตัว') }}</h2></div>

                    <div class="card-body">
                        <form method="POST" action="/updateDoctor">
                            <input type="hidden" name="id" value="<?=$doc->did?>">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="<?=$doc->name?>" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="<?=$doc->email?>" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Expertise') }}</label>

                                <div class="col-md-6">
                                    <input id="expertise" type="text" class="form-control{{ $errors->has('expertise') ? ' is-invalid' : '' }}" name="expertise" value="<?=$doc->expertise?>" required>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="<?=$doc->phone?>" required>

                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('แก้ไข') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<script src="js/jquery-2.1.1.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/gmaps.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
